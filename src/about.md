---
layout: layouts/about.html
title: About
---
Founded in 2022, No Name Games is the brainchild of Chris Bahnweg. Chris wanted to develop thematic, small-model count wargames that were perfect for people taking their first steps into miniature wargaming.

From that desire came Demigod, a little game about big ideas - like demigods trying to become full-fledged gods. Its first expansion, The Trials of Herakles, is due out in the near future.

Chris followed up the success of Demigod with Dead City, which casts players as scavengers and survivors in a world ruined by nuclear fire.

Today, Chris continues to support his existing games. He is hard at work on new rule sets to introduce new eras of history (and alternate history) to the tabletop wargaming scene.
