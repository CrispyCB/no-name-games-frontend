---
layout: layouts/product.html
title: Trials Of Herakles
coverImage: /assets/images/trialsOfHeraklesCover.svg
downloadLink: /downloads/The Trials Of Herakles Beta Rules.pdf
---

This expansion for DEMIGOD pits your band of heroes against the 12 labors of Herakles— and, eventually, against Herc himself!

This expansion includes:

- Thirteen special quests inspired by Herakles' labors

- Spoils of war inspired by the monsters of Greek myth

- Rules for having your heroes patronized by Hera, including three special boons

Please note that these rules are currently in beta and may be incomplete.
