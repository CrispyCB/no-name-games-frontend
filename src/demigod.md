---
layout: layouts/shop.html
title: DEMIGOD
coverImage: /assets/images/demigodCover.svg
PDF:
 text: "Buy A PDF Copy"
 development:
  priceId: "price_1QLAiXGVt95r9kfazbUNmVCH"
  shipping: "shr_1Pzna3GVt95r9kfamCJpZoyk"
 production:
  priceId: "price_1QWpUSGVt95r9kfaTfEB0Lkr"
  shipping: "shr_1PywoXGVt95r9kfa1qQj276v"
Print: 
 text: "Buy A Print Copy"
 development:
  priceId: "price_1QLAhoGVt95r9kfag8ChLuRS"
  shipping: "shr_1PznZeGVt95r9kfasEqAUcxm"
 production:
  priceId: "price_1QWpUJGVt95r9kfaYTDMCYZC"
  shipping: "shr_1PLnWrGVt95r9kfaO35Xkhut"
---

Ever wondered how Hercules got his start? Or Achilles? Demigod is a 28mm miniature skirmish game that lets you lead your own band of heroes through the same trials and tribulations on their way to become full-fledged gods.

The core book includes:

- Six different quests inspired by Greek mythology. Slay Medusa or the Minotaur, save the lovely Hippodamia from the Thessalian centaurs, and more!

- Rules for playing demigods patroned by Zeus, Hades or Poseidon, as well as boons for each.

- The ability to bring in minor demigods of Athena, Ares, Dionysus and all of the other gods of Greek mythology.

- A party-focused renown system, so that achievements are shared between the party and not just focused on one or two individual models.

- A growth system that focuses on actual deeds and not experience points.
