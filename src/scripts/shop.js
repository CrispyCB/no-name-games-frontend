import environment from "./variables.js";

const stripe = Stripe(`${environment.stripe_public_key}`);
// Create a Checkout Session
async function initialize(item_id, shipping_id) {
  const fetchClientSecret = async () => {
    let requestBody
    shipping_id ? requestBody = {"item_id": item_id, "shipping_id": shipping_id} : requestBody = {"item_id": item_id}
    const response = await fetch(`${environment.api_url}/checkout-session`, {
      method: "POST",
      body: JSON.stringify(requestBody)
    });
    const body = await response.json();
    return body.client_secret;
  };

  const checkout = await stripe.initEmbeddedCheckout({
    fetchClientSecret,
  });

  // Mount Checkout
  checkout.mount('#checkout');
}

const buttons = document.getElementsByClassName("stripe-button")
Array.from(buttons).forEach(button => button.addEventListener('click', () => initialize(button.dataset.price, button.dataset.shipping)))
