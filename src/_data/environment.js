require('dotenv').config();

module.exports = {
  stripe_public_key: process.env.STRIPE_PUBLIC_KEY || "Public key not set.",
  api_url: process.env.API_URL || "API URL not set.",
  environment: process.env.ENVIRONMENT || "development"
};