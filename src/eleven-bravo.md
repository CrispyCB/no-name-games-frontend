---
layout: layouts/product.html
title: Eleven Bravo
coverImage: /assets/images/elevenBravoCover.svg
downloadLink: /downloads/Eleven Bravo v0.6.1.pdf
---

*Eleven Bravo* is the result of throwing *Chain of Command*, *Crossfire* and *Force on Force* in a blender and sprinkling some modern seasoning overtop, with a little bit of beer and pretzels for flavor.

The core rulebook includes:

- Full rules for running scenarios from 1990 to now in any scale from 15 to 28mm.

- Six different scenarios as well as a scenario generator.

- Army lists for the US Army, US Marines, British, Chinese, Russians, Ukrainians and a catch-all "militia/irregulars" faction for everything in between.

Please note that these rules are currently in beta and may be incomplete.
