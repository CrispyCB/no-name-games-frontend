---
title: A Beginner's Guide To Slowfunding
layout: layouts/post.html
coverImage: /assets/images/cover-images/SlowAndSteady.png
blurb: So you've finally decided to fund your first game, but don't want to go through the rigmarole of Kickstarter. What's a game developer to do? Enter slowfunding.
date: 2023-09-07
---
So you've written your first game and decided on a price for it. Now it's time to get funding. But how? Especially as someone doing game design on an as-time-allows basis, generating funding can be hard - the usual business model, Kickstarter, requires a month-plus of dedicated time that some people just don't have.

Enter slowfunding.

## What Is Slowfunding?

Slowfunding is an alternative model where you gradually collect pre-orders for your game either until you reach some quantity of orders, or until you reach some quantity of funding. There are no stretch goals or early bird rewards - just a goal that has to be reached and, when it's reached, orders get fulfilled.
![](/assets/images/a-beginners-guide-to-slowfunding/demigodSlowfund.PNG)
So, in the example above, DEMIGOD has a $500 slowfunding goal. Once that goal is reached, I'll send out for print copies. But print copies don't have to be the only goal when it comes to slowfunding - Monkey's Paw Games is using slowfunding for the ashcan edition of [*Fiends and Fortunes*](https://monkeys-paw-games.itch.io/fiends-fortunes): in their specific case, they're using it to fund the interior art.

## Why Slowfunding?

Slowfunding is a less stressful and more flexible way of crowdfunding projects. Instead of having to rush for funding in 30 days, you have the flexibility of not having a hard deadline. There's less of a risk of complete failure and a greater chance of your project being finalized - and all of this on a timeline that's much more friendly to you, the creator.

## How Do I Slowfund?

Okay, so you've decided you want to slowfund. How do you actually set that up?

First, decide on your slowfunding goal, just like you'd decide on the funding goal for a normal Kickstarter. Usually, you'd want to tie this to something tangible, like interior art or print copies. I decided on $500 for the DEMIGOD slowfund because it was how much I needed to do a print run, for example.

Once you've decided on your slowfunding goal, decide on how you're selling your games. If you're using [Cardboard Monster](https://cardboard.monster/en-us), everything is already set up for you - Cardboard Monster's default model is slowfunding.

If you want to slowfund via Itch, it's a little different. Slowfunding via Itch is just a sale with a goal. Itch also allows you to set tiers like a standard Kickstarter - these are really just rewards that unlock at different price points.

After you've decided on your method of slowfunding, treat it just like you would any other crowdfunding effort - just on a longer timeline.

Hopefully this has opened your eyes to the world of slowfunding! If you have any questions around the process, feel free to reach out to me at <chris@no-name-games.com>.
