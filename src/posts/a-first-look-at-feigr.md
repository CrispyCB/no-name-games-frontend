---
title: A First Look At Feigr
layout: layouts/post.html
coverImage: /assets/images/cover-images/AFirstLookAtFeigr.png
blurb: A first look at Tadhg Lyons' FEIGR, a game of doomed Viking Age heroes under the watchful eyes of distant gods.
date: 2023-09-13
---

I recently got a chance to take a look at FEIGR, a Viking Age TTRPG designed by Tadhg Lyons (creator of [Wizard Pals](https://tadhgthebard.itch.io/wizard-pals) and [Raefenheim](https://tadhgthebard.itch.io/raefenheim)). FEIGR puts players in the role of Viking heroes as they live out their final days before they meet their fate. There isn't much to the system right now - just a dice system, really, but it's probably one of the more unique systems I've seen.

Each player gets 9 dice: 2 D6s, 2 D8s, 2 D10s, 2 D12s and a D20. When a player chooses to roll one of their dice, it is spent and can no longer be used. Once a player rolls their final die, their Hero's **Fated Day** arrives - they will be dead by the end of the scene. This leads to a really punchy system where players have to decide when and how to spend their dice - they might end up with just one left sooner rather than later. This is a nice way of sidestepping the way D&D and other tabletop RPGs handle skill checks - because players have a limited number of dice, rolling over and over for the same skill check is a recipe for a quick death.

The other thing that makes FEIGR different is that the Skald (GM) does not call for dice to be rolled (e.g. "Roll Perception to see if you can discern who's in the other room"). If a player says that they want their hero to do something, the Skald will either offer their opinion that a task might require a roll or say that using a roll might allow the hero to do something extra. In most scenes, the Skald's only job is to describe how events are unfolding. It is the job of the players to use their dice to interject their will onto the tale being told.

That, I think, is one of the big draws of FEIGR for me and what makes me so interested in playing it - the fact that each game is framed as a tale being told that the players are then using their dice to influence.

Beyond the dice system, both the hero creation system and the combat system are equally neat - mostly because of how refreshing and different they are.

Hero creation consists of four decisions:

- Which of the gods has marked your hero
- The gear your hero is carrying
- Your hero's defenses
- A name and identity for your heroes

At first look, this is pretty standard stuff. Heroes can choose from five different gods, each with their own boons that they offer the players. It's the gear and defense options that make the hero creation really shine. All of the gear is evocatively described, and the way it interacts with the game world only serves to draw the player in. Heroes begin with two defenses - descriptive items that might refer to concrete belongings or a character's own natural talents.

Defenses also tie into the combat system - when your character loses all of their Defenses, they are Wounded, which has negative effects both in and out of combat.

Combat is played out in a series of Stanzas, repeating turn sequences that alternate between the heroes, their allies and their enemies. From the first character's turn, battle follows this procedure:

- After a Hero turn, if the Act they took was successful play goes to an Enemy.
- If the Hero's Act was a failure (or if they do not act), play moves to an ally of their choice.
- An Enemy turn always passes to a Hero (except in the case of certain types of enemies)
- Once every combatant has had a Turn, that completes a Stanza, and characters who have acted can be chosen to go next again.

This whole sequence feels a lot more evocative to me than the bog-standard "roll initiative and act in initiative order" that most other RPGs follow. Having the combat sequence bounce around between players every turns makes combat feel a little more real than just going down a list.

All in all, FEIGR introduces a lot of new concepts that I'm excited to playtest in the coming weeks. If you're looking for an RPG that rocks the boat, definitely give FEIGR a try.
