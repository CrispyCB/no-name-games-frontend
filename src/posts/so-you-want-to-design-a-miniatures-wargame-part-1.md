---
title: "So You Want To Design A Miniatures Wargame: Part One"
layout: layouts/post.html
coverImage: /assets/images/cover-images/MiniaturesWargame.png
blurb: "Inspired by games like Star Wars Armada, you've decided to write your own wargame. But where do you begin? How do you write a game as complex as that from scratch?"
date: 2024-06-19
---
So you've heard that [*Star Wars Armada* is going away](https://www.atomicmassgames.com/transmission/update-on-star-wars-x-wing-and-star-wars-armada/), and you want to write a your own game of grand fleet battles for you and your friends who are fellow Star Wars fans. But where do you begin? How do you write a game as complex as *Star Wars Armada* from scratch?

## Start With Someone Else's Rules

No, really. Read as many tabletop wargaming rulesets as you can. Read them again. Read tabletop RPGs, too - the first tabletop RPGs started as offshoots of tabletop wargames, after all. You'll either find mechanics that you want to repurpose in your own work, or they'll inspire your own ideas.

Once you've got a good grasp of what makes a wargame a wargame, the easiest way to start writing your own is by house-ruling the games you already play. Maybe all X-Wings have +1 on attack rolls against Empire capital ships, for example. Start with small rules changes and slowly work your way up.

Modifying rules is a great exercise that teaches all the basics of rules writing without burdening yourself with the mental load of trying to come up with a complete game system from scratch. It's a lot easier to take a game that you love 90% of and modify it until you love all 100% than it is to write that 100% from the ground up.

## The Two Ws

Back from houseruling every game you can find? Still want to write your own game? The biggest question to ask yourself before you begin is "Why?" Why do you want to write a miniatures wargame? For most of us, it's probably because the game you want to play doesn't exist, but other reasons might include:

- You've created a range of miniatures and you want to create a game to help sell them
- You've written a setting/IP and you want to give players a way to explore the setting you've created
- You want to get your name in print
- Just because!

Figuring out why you're writing a game is important because the decisions you make when you're writing a game to sell a line of miniatures are very different from the ones you'll have to make if you're just making the game so you have something to scratch a particular gaming itch.

Once you've decided why you're writing a game, figure out who you're writing it for. Games that appeal to die-hard wargamers are written very differently from ones that appeal to newbies, ditto if they're people that have only played games from bigger manufacturers like Games Workshop. And, of course, you can throw most of those considerations out the window if you're just writing this for yourself or your gaming buddies.

## Creative Constraints

Now that you've decided who you're writing this for and why, you have to decide on your creative constraints - what are your limits when it comes to designing this thing? It can be tempting to say you have no creative constraints, but that's a really easy way to spend tons of time at the drawing board with nothing to show for it.

Some common things to think about include:

- Whether you're designing just digitally or for print as well - print layouts are their own separate beast from digital ones, and what looks good digitally may look horrible in print and vice versa- we'll talk about this in another post.
- Is this a one-off, or do you plan to write expansions?
- Do you have a budget for art and layout? What parts of this process can you do yourself?
- Are you just writing a rulebook, or are you planning to make miniatures and other accessories too? ([This](https://web.archive.org/web/20220516182123/http://massiveawesome.com/miniature-myth-busting/) post from Massive Awesome is a couple years out of date, but it does a great job of outlining the costs of getting even a single miniature to production)

If you're writing your game because you want to sell it (whether yourself or to a publisher), the questions above will become the core formation of your business plan.

Once you have the answers to those questions, it's time to dive into actually designing the game, which is where we'll pick up with the next post in the series.
