---
title: Demigod Returns From Kickstarter!
layout: layouts/post.html
coverImage: /assets/images/cover-images/ReturnedFromTheUnderworld.png
blurb: DEMIGOD has officially come out of Kickstarter. So what does this mean for the future of the project?
date: 2023-08-30
---
As of last week, DEMIGOD is officially out of Kickstarter. Unfortunately, it didn't meet its crowdfunding goal. While it always sucks when this happens, never fear: I have a cunning plan.

If you take a look at the [DEMIGOD Itch page](https://no-name-games.itch.io/demigod), you'll notice that DEMIGOD is now set up to slowfund. I talked about this a little in [the post from two weeks ago](/posts/exciting-new-developments/). So what's changed from then to now?

Well, I'll no longer be slowfunding via Cardboard Monster. I didn't know that Itch allowed you to [offer physical goods](https://itch.io/docs/creators/exclusive-content#how-you-can-use-rewards/selling-physical-goods) when it came to setting up their reward tiers. Now that I know that that's the case, I no longer need Cardboard Monster - everything will be handled by Itch.

That being the case, the reward tiers for Itch will mostly be carry-overs of the ones from Kickstarter. The "Mortal Acolyte" tier will go away, as will the larger tiers with the 3D printing files. Instead, the Itch tiers will be broken down like this:

- **Digital Demigod**: Gets you a full-color PDF copy of the DEMIGOD rulebook. $15.
- **Seasoned Campaigner**: Gets you a full-color PDF copy of the DEMIGOD rulebook, plus *The Suitors For Lady Penelope* add-on scenario. $18.
- **Scion Of Olympus**: Gets you full color print and PDF copies of the DEMIGOD rulebook, plus *The Suitors For Lady Penelope* add-on scenario. $50 + shipping.
- **Patroned by Hermes**: For merchants, retailers and others patroned by Hermes. Gets you four print copies of the DEMIGOD rulebook, as well as PDF support for any physical book sales you make going forward. $100 + shipping.

Each slowfund I set up will cap at $500 - enough for me to do a print run of the DEMIGOD core book to fulfill the orders placed during the slowfund. Once I reach that cap, the print order gets placed and the next slowfund begins.

If you have any questions about the slowfund tiers or the process as a whole, feel free to reach out to <chris@no-name-games.com> with your Qs.
