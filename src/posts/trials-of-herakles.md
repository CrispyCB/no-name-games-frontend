---
title: An Introduction To The Trials Of Herakles
layout: layouts/post.html
coverImage: /assets/images/cover-images/TrialsOfHerakles.png
blurb: "'The Trials of Herakles', DEMIGOD's first expansion, is officially live to the public! Want to know more? Read on!"
date: 2025-01-27
---

Have you ever wanted to kick Herakles' ass? [NOW YOU CAN](https://no-name-games.com/trials-of-herakles/).

Welcome to the "The Trials of Herakles".

## So what is "The Trials Of Herakles"?

Put simply, "The Trials Of Herakles" is a twelve-scenario mission pack that reimagines each of Herakles' twelve labors through the lens of encounters in DEMIGOD. You can slay the Nemean Lion, try your best to clean out King Augeus's stables, pit your wits against Hippolyte of the Amazons and more. The book is designed with solo play in mind (as is the DEMIGOD core rulebook), but I am playtesting a set of profiles to allow a second player step into the sandals of Herakles himself and really see if they can get the labors done before your adventuring party does.

## What else is in the book?

Besides the twelve labors, you'll have three different boons from Hera, allowing her to be your demigod's patron if you so choose. You'll also have access to the bestiary at the back of the book, meaning you can add the monsters you encounter in "Trials of Herakles" to your monster hunts in normal games of DEMIGOD.

## What do I need to play?

Besides a copy of the "Trials of Herakles" beta rules (available [here](https://no-name-games.com/trials-of-herakles/)), you'll also need a copy of the DEMIGOD core rulebook (available here from my own site, or here as a PDF from Wargame Vault), as well as some miniatures to represent both your own adventuring party and the monsters you'll encounter throughout "The Trials of Herakles". For a general primer on what miniatures match the "vibe" of DEMIGOD, check out [this post](https://no-name-games.com/posts/mano-a-monster/) I made a couple weeks ago on the subject.

If you're looking for miniatures specific to "Trials Of Herakles", so far I recommend [this lion](https://cults3d.com/en/3d-model/game/lion-dnd-miniature-tabletop-miniatures-gaming-monster-3d-model-rpg-d) by Epic Miniatures. I'll keep this post updated as I work through the other 11 scenarios, so if you're looking for inspiration for tackling the quests from "Trials of Herakles", don't forget to check back!
