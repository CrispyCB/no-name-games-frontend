---
title: Exciting New Developments
layout: layouts/post.html
coverImage: /assets/images/cover-images/RegularlyScheduledProgramming.png
blurb: The Blog With No Name's been on a bit of a hiatus while I work through some exciting new developments. Read on to find out more!
date: 2023-08-14
---
So The Blog With No Name (and No Name Games as a whole) have been on a bit of a hiatus for a couple reasons. First was that I [started a Kickstarter](https://www.kickstarter.com/projects/no-name-games/demigod-0) for DEMIGOD, No Name Games's first solo-published game. When it looked like that Kickstarter wasn't going to fund, I turned to setting up other ways for getting the game funded and "paying myself back". So far, I've arrived at three solutions:

1. Slowfunding via Itch.
1. Slowfunding via [Cardboard Monster](https://cardboard.monster/en-us).
1. Some combination of the above.

As it stands, I'm looking mostly at #3. Itch would handle the distribution of PDF/digital versions of my games and their associated 3D printing files, while Cardboard Monster would handle physical distribution. So here's how things will break down:

- If you want a PDF copy of DEMIGOD and/or the 3D print files, those will be available from Itch for $15 (PDF) or $10 apiece for the 3D printing files.
- If you want a print copy, you'll have two options: order straight from me for $30; or order from Cardboard Monster for $30.

Speaking of physical copies, that leads to my second point: as of the end of next month, I'll have physical copies of DEMIGOD in [Nowhere's Store of Forgotten Lore](https://www.nowheresstore.com/) in Springfield, MO. So if you're in the Springfield area, definitely check it out!
![](/assets/images/exciting-new-developments/NowheresCoverImage.jpg)
The third thing doesn't really have to do with the first two, but I also [redesigned the No Name Games website](https://no-name-games.com/)! I got rid of the big central logo in place of something a little more understated. The click effect is also gone on the logo. Makes the site a little longer, but now everything should load faster - especially because the site and blog are now rolled into one.

I'll be back next week with our regularly scheduled programming - a post on how to price your TTRPGs.
