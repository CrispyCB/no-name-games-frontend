---
title: A Quick Guide To Getting Into Miniature Wargaming
layout: layouts/post.html
coverImage: /assets/images/cover-images/FightingLittleWars.png
blurb: When it comes to miniature wargaming, there are a dizzying number of creators and games. Where's an interested player to start?
date: 2023-06-26
---

Want to get into wargaming, but have no idea where to start? The explosion of the hobby in the 2000s has led to the creation of a dizzying number of new miniatures creators and new games to go with them. So where’s an interested player to start?

## Find A Game Store Near You

Wandering into the friendly local game store (FLGS) or comic shop is often most people’s first encounter with miniatures wargaming. Punching in a Google search for “tabletop gaming stores near me” will give you a good list. Retailers such as Games Workshop and Warlord Games usually have store locators that you can search, too - popping down to one of the stores on the list is a great way to get introduced to the hobby.
![](/assets/images/getting-into-miniature-wargames/warhammer-game.png)

## What If You Can’t Find An FLGS?

While wargaming started as a two-player (or more) hobby, more and more companies are releasing games or supplements that cater to the solo wargamer. Osprey Games released [Rangers of Shadow Deep](https://www.modiphius.net/en-us/collections/rangers-of-shadow-deep) in 2018, a dark fantasy-inspired solo game set in a fantasy world created by Joseph McCullough. McCullough’s fantasy skirmish game Frostgrave also has an expansion, Perilous Dark, that focuses on solo play. Other games and supplements include Kontraband for Patrick Todoroff’s Zona Alfa, Five Leagues From The Borderlands (and the sci-fi version Five Parsecs From Home), Perilous Tales, Fallout: Wasteland Warfare, Skyrim: Call To Arms.

I’ve also released my own solo wargame, a Greek-mythology-inspired skirmish game called [Demigod](https://no-name-games.com/demigod).

## Pick Miniatures That You Like

Once you’ve found a place to play, the next step is to find miniatures that you actually want to play with. There are miniatures out there for nearly every period of history - from the truly ancient to more modern wars and even the far-flung future. Finding miniatures that you like is a great way to keep yourself in the hobby - after all, miniature wargaming grew out of the hobby of collecting model soldiers.
![](/assets/images/getting-into-miniature-wargames/miniature-soldiers.jpg)

## Already Have Miniatures? No Problem

More and more, miniatures wargaming is moving toward the concept of “miniature agnosticism” - that is, the fact that you don’t need to use a company’s boutique miniatures to play their games. And some companies are taking it even further than that - releasing just the games themselves and expecting players to supply the miniatures. Examples include [Crossfire](https://crossfire.wargaming.info/) (platoon-level WW2 gaming), as well as every single ruleset [One Page Rules](https://www.onepagerules.com/) has put out.

## Play The Games You Want To Play

Once you’ve got miniatures and a place to play, decide on what game you want to play. Rules exist for almost any setting and scenario you can think of, from ancient warfare to fantasy skirmishes. Want to replay Caesar’s conquests in Gaul? Check out Warlord Games’ [Hail Caesar](https://store.warlordgames.com/collections/hail-caesar). Refight the American Revolution with Black Powder. For fantasy systems, pick between Warlords of Erewhon, [Kings of War](https://www.manticgames.com/kings-of-war/getting-started/), Oathmark and more. In terms of sci-fi, the big boy on the block is Warhammer 40,000, but others exist too - including Mantic Games’ Warpath.

The most important tip I have for you, though, is this: get out there and play! Even if you don’t own exactly the right miniatures or they aren’t painted in exactly the right colors, what matters is that you’re out there and playing. Miniature wargaming - whether solo or against an opponent - is a great way to explore your interests and meet new people, to boot! So get out there and play on!
