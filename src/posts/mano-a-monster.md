---
title: Miniature Range Options For DEMIGOD 
layout: layouts/post.html
coverImage: /assets/images/cover-images/ManoEMonster.png
blurb: Since releasing DEMIGOD in November of 2022, I've received a number of questions on where to get fitting miniatures to represent your demigods and monsters.
date: 2025-01-01
---
Since releasing DEMIGOD in November of 2022, I've received several questions on where to get fitting miniatures to represent your heroes, minions and monsters - so much so that I've actually put a whole channel on the [DEMIGOD Discord](https://discord.gg/ezTvXG24V8) just to address the question.

DEMIGOD is a skirmish wargame set against the backdrop of Greek mythology, playable with as few as four figures on the board. It is manufacturer agnostic and the rules are flexible enough that you'll find equivalent models for almost every model in the book in the catalogue of any company that makes an "Ancient Greeks" range.

With that out of the way, let's talk manufacturers. There are a TON of miniatures manufacturers out there, and many of them make miniatures that would be compatible with DEMIGOD - perfect for representing your heroes, minions and monsters, but I'm just going to talk about the ones I've used for my own collection.

## Heroes and minions

Since your heroes and minions are going to be constantly rotating through your warband, it's important to have a lot of them! Let's take a look at the best ways to fill out your adventuring party.

### Victrix

Victrix's multipart kits are the absolute bang for your buck when it comes to building a DEMIGOD warband - so much so that they're one of two manufacturers (the other being Warlord Games) that I point to when people ask what they need to get started with DEMIGOD. In addition to your bog-standard hoplites (perfect for your demigods themselves as well as, well, hoplites), Victrix produces a variety of kits that build almost every kind of minion you'd want to hire in DEMIGOD.

Now, for the purposes of a small-model-count game like DEMIGOD, the Victrix kits are a little overkill. The newest hoplite kit contains 48 figures, which is enough to build 16 warbands at the minimum warband size.
![](/assets/images/mano-a-monster/greek_hoplites.png)
Additionally, the new kit seems tailor-made for skirmish games. Each sprue comes with seven different bodies plus a variety of different arms and heads, allowing you to create a warband that is dynamic and characterful even if your weapon choices are limited to swords, spears and shields (and a severed head!).
![](/assets/images/mano-a-monster/peltasts.png)
The peltast kit will fill out your ranged troops - giving you, as the box itself says, a range of peltasts, javelinmen and slingers. This gives you a variety of ranged options, from long-range slings to the peltast's spear, which can cripple an enemy model as it attempts to close.

The last kit I'd recommend from Victrix is their "Greek Unarmored Hoplites and archers" kit, which gives you 48 hoplites in tunics and eight different archer options - perfect for novice demigods with not a lot of coin to their name or a minor hero of Apollo, the god of archers - or even just an archer minion himself!
![](/assets/images/mano-a-monster/archers.png)
About the only thing I'd call out with the last two kits is that the detail on them feels a little soft when compared to the new hoplite kit. Hopefully we see a re-release of these kits with some better detail!

### Wargames Foundry

Metal miniatures manufacturers that also hail from Great Britain, Wargames Foundry (WGF) is the brainchild of Cliff and Brian Ansell. For those of you who are Games Workshop grognards like me, Brian Ansell is also one of the founders of Citadel Miniatures, which later became part of Games Workshop.

All of WGF's kits are 1 to 3-piece metals: depending on which kit you get, you'll have to attach a spear, a shield, neither, or both. In terms of the spears themselves, I recommend replacing the included spears with the [NSS102 spears](https://www.northstarfigures.com/prod.php?prod=537) from North Star Military Figures, Artizan Designs, or Crusader Miniatures - they're a little thicker and fit the models' hands better, although they do need trimmed to size.

This makes them really fast to get onto the table - there's not a ton of assembly required. At the same time, the poses tend to be rather generic, and some of the hoplite poses take a little finagling to get the spears to sit properly.

Almost all of the sub-ranges under WGF's "World of the Greeks" range are suitable for DEMIGOD - in fact, the only range that I'd say _isn't_ suitable is the Thracians, and even then only because they don't fit the "hoplite aesthetic" of the rest of the range.

Perhaps my favorite kit is WG162 "Herakles and the Argonauts", which features 5 models with swords and shields plus a sixth representing Herakles himself - perfect for a Major Hero plus several Minor Heroes and the Hero of Legend, Herakles!
![](/assets/images/mano-a-monster/herakles.png)

### 3D Breed

The newest addition to my collection and the only non-physical hero/minion models on this list, 3D Breed's "March to Hell: Rome" range is perfect for games of DEMIGOD. Not only do they have a [sample hoplite](https://www.3dbreed.es/print-test/), but the whole range should get your DEMIGOD warband pretty well stocked.

You can find STLs for Jason and the Argonauts, plus hoplites (both in armor and tunic), psiloi (slingers), peltasts and Cretan archers. There's also a separate, smaller range for Spartans that'll give your warband a little flavor with Spartan hoplites and an STL for Leonidas himself - perfect for your Major Hero!
![](/assets/images/mano-a-monster/leonidas.png)

## monsters

When it comes to the monsters that stalk through the annals of Greek antiquity, a 3D printer is going to be your best friend. While there are a couple metal and plastic miniatures out there, (WGF has medusae and a kit that includes Theseus, the Minotaur, and some pillars, while Reaper's metal Bloodhoof miniature is a particular treat), the vast majority of Greek monstrosities are found in the land of liquid resin.
![](/assets/images/mano-a-monster/minotaur.jpg)
The [Medusa](https://cults3d.com/en/3d-model/art/medusa-fidad) by ArtOdyssey over on Cults3D was the one I ended up using for my demo game of "The Garden of Statues" at HMGS Fall In this past year, and it's probably the only one I like out of the ones I've found - that is, it's the only one that isn't gigantic and doesn't come with added weapons.

There's one other model I'd like to call out, just because of how awesome it looks: the Cyclops by "clynche art" over on MyMiniFactory. One of the Discord users printed it and posted a picture that came out looking so awesome.
![](/assets/images/mano-a-monster/polyphemus.jpg)
There are way more options than these when it comes to models that mesh with Greek antiquity. One of my goals when I wrote DEMIGOD was to let my players step into the sandals of some of the heroes of Greek mythology, so go forth and find the models that speak to your hero's soul!

[Buy DEMIGOD from No Name Games](https://no-name-games.com/demigod/).
