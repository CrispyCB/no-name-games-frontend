---
title: What's My ATK Again?
layout: layouts/post.html
coverImage: /assets/images/cover-images/WhatsMyAtkAgain.png
blurb: Having a quick-start guide is one of the best ways to get people up and going with your game. But how do you write a good guide?
date: 2024-01-31
---
When it comes to first introducing someone to a new wargame, a quick-start guide is essential for getting up and going. Quick-start guides offer a gradual start into a particular wargame, giving players just enough rules to get them gaming without overwhelming them with minutiae. So what do you need to consider in order to write a good quick-start guide?
## Consider Your Audience
The first thing you need to do is consider your audience. How much wargaming experience do they have? Are they familiar with your game and its conventions or just wargaming in general? A good way to strike the balance between an audience that knows your game and one that doesn't would be to include a 1-2 page blurb (with plenty of pictures!) that introduces the reader to your game world. It will serve to draw in those who are unfamiliar with your game world, but those who are already familiar can easily skip it. Likewise, if your quick-start guide is bundled with multi-part plastic miniatures (as most of the quick-start guides from Games Workshop are, for example), including an assembly guide is always helpful.
## Decide How To Frame Your Content
A quick-start guide should start small and build up so that players learn the rules in manageable chunks. There's two main ways I've seen this presented: 
1. Present a blocked-out overview of the game table, with each block going through some different aspect of the game.
![](/assets/images/whats-my-atk-again/YourFirstGame.png)
2. Design individual missions, with each mission adding on to the mission before it until players are using most of the rules in a single mission.
![](/assets/images/whats-my-atk-again/FirstMission.png)

Blocked out overviews are great for covering all aspects of the game rules that a new player might be curious about. They take up relatively little space, too (the Battle For Black Reach overview is a two-page spread), meaning that you can pack your quick-start guide with other content such as model assembly guides or painting tutorials.

Conversely, linked missions are better for learning the actual rules since they start players with the absolute rules basics (usually moving and attacking) and then build up from there. For example, the Battle For Macragge quick-start begins with movement and shooting and then adds assault rules in the third mission. The second and fifth missions also expand the shooting rules, adding flamer and blast templates.

Once you've decided between a blocked-out overview and a linked mission list, the next thing to do is consider layout.

## What About Layout?
The best quick-start guide in the world won't get eyes on it if it's not laid out well. So how do you properly lay out a quick start guide? There's two big things to keep in mind:
1. Clear, consistent headings are important. Choose a single font for all of your chapter titles, a single font for all your subheadings, etc, and stick to it. This consistency makes your quick-start guide easy to read and easy to follow, which are the two most important qualities of any QSG.
2. Annotated screenshots trump text. Showing game mechanics through a screenshot of the board along with a small text box is much more visually appealing than the same concepts explained via paragraphs of text.
![](/assets/images/whats-my-atk-again/graphic.png)
For example, the screenshot above shows that when spore mines detonate, they hit everything under a small blast template. This is much better shown visually by physically staging a spore mine under a small blast template and taking a picture.

## Make Things Stand On Their Own

Once you've decided how to frame your content and decided on layout, the last thing you need to consider is how much content to include. You want your quick-start guide to stand on its own - that is, a prospective player should be able to pick up your quick start guide and, by the time they've read/played through it, they should be able to play an actual full gameplay loop of your game without necessarily consulting the rulebook.

Consider your gameplay loop. What are the main phases of your game? Ensure that each phase is included somewhere in your quick reference guide. The best way to do this is to slowly build them into subsequent loops, where each loop expands on the previous loop. So the first gameplay loop might be movement and shooting; the second might be movement, shooting and close combat; the third might add on morale tests, too. The goal of this is to use steadily-increasing gameplay loops to gradually introduce your game's concepts to your players so that they are not overwhelmed. For someone new to wargaming, having to deal with movement, shooting, close combat and morale checks for multiple units can be dizzying - much better to start with as few models and as few concepts as possible.

Decide how many mini-gameplay-loops you need to introduce your gameplay concepts. Usually, you'll have as many mini-loops as you have concepts, with the end goal being that the last mini-gameplay-loop is in fact a full gameplay loop - each of the concepts of the previous loop has been included in it. 

Include a hook. Once players finish your quick start guide, their immediate question is usually "what do I do next?" Including a section that answers these questions is a great way to keep your players engaged with your game.

## In Conclusion

So, here's how you write a good quick start guide for your next wargame:
1. Consider your audience, their wargaming experience and their experience with your games.
2. Decide on how to frame your content. Using linked missions that steadily increase the number of game rules shown to players is usually best.
3. Use clear, consistent headings.
4. Turn your rules explanations into annotated screenshots where possible.
5. Include a hook to draw prospective players further into the hobby.

