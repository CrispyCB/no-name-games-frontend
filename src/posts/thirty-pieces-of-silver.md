---
title: Thirty Pieces Of Silver
layout: layouts/post.html
coverImage: /assets/images/cover-images/ThirtyPiecesOfSilver.png
blurb: Or, how to properly price your TTRPG or tabletop wargame without selling a piece of your soul.
date: 2023-08-23
---
So you've poured sweat, tears and (hopefully not) blood into the creation of a new TTRPG or wargame. You've got some folks on the internet interested in buying a copy - and maybe even a few physical stores, too. It's time to put a price on the thing. But how do you actually do that without selling it for too much (or too little)?

1. Itemize your costs.

![](/assets/images/thirty-pieces-of-silver/expenses.png)
Before you do anything else, figure out how much it cost you to get this book made. Make sure you keep track of as many dollars as possible. Art? Keep a note. Editing? Keep a note. Layout? Keep a note. This will tell you just how much money you need to "pay yourself back" in order to start turning a profit on your game. So, for example, I spent $2338 bringing DEMIGOD to life - a combination of illustrations, editing fees and general business costs. Before I can start turning a profit on DEMIGOD, I need to make back that $2338. Additionally, those costs can be divided into what I call "spin up" costs and "keep the lights on" costs. Your spin up costs are all of the costs you incur to bring your product to market, while your keep-the-lights-on costs are just that - the costs of doing business and keeping the lights on. Keep-the-lights-on costs would be things like website or email hosting costs, printing costs, convention costs, etc.

1. Determine your cost-per-item.

Once you've figured out what your costs are to bring your game to market, you need to figure out your cost-per-item. The best way to do this is to take your total costs and divide it by your wholesale stock. So if you have $2338 in costs and 100 units of your game in stock, your cost-per-item is $23.38. You will need to sell your game for at least $24.00 (rounded up for convenience) in order to break even on your costs.

1. Do proper market research.

![](/assets/images/thirty-pieces-of-silver/games.jpg)
Now that you have your cost-per-item, it's time to do some market research. Look up what games like yours are selling for. No, that doesn't mean "demand Player's Handbook prices just because you're also selling a fantasy RPG a la D&D". Instead, look to the smaller market players - something like [Castles and Crusades](https://www.trolllord.com/tlgstore/#!/The-Game/c/119538267) (which retails for $19.99 as a PDF and $39.99 as a physical book) or [Low Fantasy Gaming](https://www.drivethrurpg.com/product/265388/Low-Fantasy-Gaming-Deluxe-Edition?src=hottest_filtered) (which retails for $19.95 as a PDf and $44.95 as a physical softcover). Since I'm designing a wargame, I used the [Osprey Games](https://ospreypublishing.com/us/osprey-games/wargames/) website (among others) to do my market research. Wargames of the kind I was designing retailed for about $35-40 for hard copies and around $20 for PDFs, giving me a good amount of wiggle room when it came to pricing my own game.

1. Decide on both print and PDF prices.

![](/assets/images/thirty-pieces-of-silver/prices.png)
When it comes to pricing out your game, make sure you set separate prices for both print and PDF copies. While both formats have their associated costs, PDFs do not have the costs of printing and shipping, and so can typically be priced lower - usually 50% of the print retail price. This also means that you can price print copies to absorb the cost of their own printing and shipping, while PDFs are pure payback/profit.

1. Set a Suggested Retail Price (SRP).

When businesses express interest in stocking your game, sometimes they'll ask for a Suggested Retail Price (SRP) - that is, how much they should charge for the book when someone comes to their store to buy it. Your SRP is your cost-per-item plus your profit margine (which is itself a percentage of your cost). Or, expressed as a formula:

> SRP = Cost + Profit Margin

You want to give any retailer who stocks your game _at least_ a quarter of the cost as profit margin (including yourself!), if the market will bear it. So if DEMIGOD costs me $24.00 to produce, then 25% of 24 ($6.00) is my profit margin, leading to a total minimum SRP of $30.00 - for print copies, at least. Since PDFs are usually priced at 50% of the retail price, the PDF price for this example copy of DEMIGOD would end up being $15.00.

Pricing your game can seem overwhelming, but this should give you a general ballpark and provide some insight into how things work "behind the scenes". If this sort of thing piques your interest or you have any other tips on pricing games, let me know via email at <chris@no-name-games.com>.