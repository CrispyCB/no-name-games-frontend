---
title: A Brief History of Miniature Wargames
layout: layouts/post.html
coverImage: /assets/images/cover-images/FromKriegsspielToYourKitchenTable.png
blurb: From H.G. Wells to GW and beyond. Or, why a post with "Kriegsspiel" in the title doesn't actually talk about Kriegsspiel.
date: 2023-06-25
---

## The First Miniature Wargame (UK, 1913)

![](/assets/images/a-brief-history-of-miniature-wargaming/little_wars.jpg)
Developed by the English writer H.G. Wells, Little Wars was the first book published on miniature wargaming. Unlike modern wargames, Little Wars was designed for a much larger playing field - like an entire living room floor. Infantry moved a foot (measured by a piece of string), while cavalry moved two. When units engaged in combat, they suffered a predetermined amount of losses based on how large they were.

Despite being designed for ease of play and accessibility, Wells’ rulebook failed to invigorate the miniatures wargaming community for a number of reasons: the rationing of tin and lead during the first World War made model soldiers expensive; there was also a dearth of clubs or magazines or even players of miniature wargames, which were still seen as a niche within the larger hobby of collecting toy soldiers.

## The First Miniatures (US, 1955)

Wargaming miniatures as we know them today owe their start to John Edwin “Jack” Scruby, who began casting them out of type metal and selling them in his central California shop. Each miniature cost 15 cents, or about $1.68 in 2023 US dollars. By 1963, Scruby had switched from type metal to the 50/50 tin-lead alloy that would remain a wargaming staple well into the 1990s.
![](/assets/images/a-brief-history-of-miniature-wargaming/scruby.jpg)

Scruby’s models are still being produced today by [Historifigs](https://historifigs.com/), who offer both the 40mm and 54mm variants of Scruby’s designs.

In addition to his work with wargaming miniatures, Scruby ran the first US miniatures wargaming convention in 1956. The following year, he launched War Games Digest, a magazine devoted to gaming with military miniatures. WGD was published quarterly until 1963, at which point Scruby began publishing Table Top Talk, a sort of proto-White Dwarf (that is, a hobby magazine centered around selling his miniatures and miniature rulesets). One of the volumes of TTT can be read [here](https://drive.google.com/file/d/1YMNmDr1AI6wMlM8_JJf94L_YjDgUjrH1/view).

## Don Featherstone’s War Games Battles and Manoeuvres with Model Soldiers (UK, 1962)

More commonly known simply as War Games, Don Featherstone’s book on miniature wargaming (which included Tony Bath’s 1956 ancient wargaming ruleset) is considered both the first mainstream publication for miniature wargaming since Little Wars but also the father of what we would now consider modern wargaming.

Featherstone’s book (along with the emergence of several British miniature figure manufacturers such as Skytrex and Peter Laing) was responsible for a huge upswing in the popularity of the hobby throughout the 1960s and 1970s.

## Chainmail (US, 1971)

![](/assets/images/a-brief-history-of-miniature-wargaming/chainmail.jpg)

Created by Gary Gygax, Chainmail drew its inspiration from both Tony Bath’s earlier medieval mass combat ruleset and also the rules designed by Jeff Perren, who used Henry Bodenstedt’s Siege of Bodenburg medieval combat rules as an inspiration.

Chainmail’s big contribution to miniature wargaming was its set of mass-combat rules. In Chainmail’s ruleset, each figure represented twenty men, and figures are divided into six basic types: light foot, heavy foot, armored foot, light horse, medium horse, and heavy horse. Depending on which unit was attacking which, players rolled a number of six-sided dice, with certain values denoting a kill. For example, when heavy horse is attacking light foot, the attacker is allowed to roll four dice per figure, with each five or six denoting a kill.

Chainmail also included a fantasy supplement, which included rules for spells, magic weapons, and heroic units - an early precursor to both first-edition Dungeons and Dragons and Warhammer Fantasy Battle.

## Warhammer Fantasy Battle (UK, 1983)

![](/assets/images/a-brief-history-of-miniature-wargaming/warhammer.png)
Developed by Bryan Ansell, Richard Halliwell and Rick Priestley, Warhammer Fantasy Battle (WHFB) was a miniature wargame published by the Games Workshop company from 1983-2015. WHFB came into existence out of a desire to use otherwise-unused fantasy miniatures, and the first edition rulebook contained both mass-battle rules as well as general rules for roleplaying. By the third and fourth editions, in 1987 and 1992, the roleplaying rules had been mostly removed in favor of fantasy mass-battles.

Third edition also heavily-encouraged the use of standardized army lists, as opposed to the more open-ended army design of the first and second editions. By fourth edition, army lists were enforced in the form of Warhammer Army books for each of the game’s separate factions. Each book gave its faction a limited number of unit options: army list composition was further limited by the number of points that could be spent on troops, monsters, heroes and so on.

By sixth edition in 2000, WHFB had become the fantasy mass combat game that most people are now familiar with. The game would be sunset by Games Workshop in 2015 (on its eighth edition as of 2010) citing a decrease in sales.

## Warhammer 40,000 (UK, 1987)

Perhaps the most popular miniature wargame in the world, Warhammer 40,000 began life as an offshoot of WHFB. Rick Priestley and his fellow designers had added a smattering of optional science fiction elements, namely in the form of advanced technological artefacts (e.g., laser weapons) left behind by a long-gone race of spacefarers, to the WHFB universe.

These science-fiction elements coalesced with another ruleset that Priestley had begun writing before he joined Games Workshop, a spaceship combat ruleset by the name of Rogue Trader. The lore of Priestley’s game remained in the first edition of Warhammer 40,000 (released as Warhammer 40,000: Rogue Trader due to concerns that it would be confused with a Rogue Trooper board game also produced by Games Workshop) but the spaceship rules were cut due to lack of space.

The initial Rogue Trader rules were more heavily oriented toward roleplaying vs traditional wargaming. It was not until the game’s second edition in 1993 that army lists were introduced, mirroring their introduction in WHFB a year earlier. Armies in Warhammer 40,000: Second Edition had to spend 75% of their points on units from the same faction. Up to 50% of an army’s points could be spent on a single hero, giving rise to the nickname of “Herohammer”. Players could choose from nine codexes/army books: Angels of Death, Chaos, Eldar, Imperial Guard, Orks, Sisters of Battle, Tyranids, Ultramarines and Space Wolves. The Angels of Death, Ultramarines and Space Wolves codexes each covered different army lists for the Space Marines faction.

The third edition of the rules streamlined them to allow for much larger battles. Armies in third edition continued the use of codexes. Four new army codexes were introduced toward the end of the edition: two new alien races (the Necrons and the Tau) and two armies of the Inquisition - the Witchhunters and the Daemonhunters (previously featured in the Sisters of Battle codex and other, supplementary material).

From sixth edition (2012) onward, the Warhammer 40,000 system was plagued with rules bloat, something that Games Workshop hopes to fix with the release of the game’s 10th edition in 2023.
