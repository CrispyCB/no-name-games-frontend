---
title: "So You Want To Write A Miniatures Wargame, Part Two"
layout: layouts/post.html
coverImage: /assets/images/cover-images/MiniaturesWargamePartTwo.png
blurb: "Sometimes the best way to design a game is to start at the end. What do you want these rules to do? A quick breakdown on how to build a gameplay loop and what playtesting might look like."
date: 2024-07-17
---
So in the [previous post](/posts/so-you-want-to-design-a-miniatures-wargame-part-1/) in this series, we talked about some of the basic things that you should think about when you design a miniatures wargame, such as who you're writing it for, why you're writing it, and some of the creative constraints you might need to keep in mind as you start designing your game. But how do you actually design a game?

## Work Backwards To Go Forwards

It might sound a little counter-intuitive, but sometimes the best way to design a game is to start at the end. What do you want these rules to do? Identify the overall feeling or "vibe" you want for your players - e.g. "I want each player to command a fleet of starships." Once you've decided on the overarching vibe, the next thing to do is to figure out the important actions you want your players to do in support of that overall vibe.

For example, if the overall goal of your players is to command a fleet of starships, then the important actions for your gameplay loop might be "issue orders to your ships" and "deal with command friction".

## Create Your Gameplay Loop

Once you've decided on the important actions for your players, it's time to break them down into a gameplay loop - the repetitive actions that your players take when they're playing the game. So, taking "issue orders to your ships" as our first example, you have to decide:

- What orders can you issue to your ships?
- What are the conditions in which you can issue these orders?

The orders you can issue to your ships might be, at their simplest: move, shoot, release fighters, and repair damage. Let's say that a ship can move 6" in a forward direction and turn once during that movement. That's your movement taken care of.

Shooting is similarly generic: ships can shoot at other ships as long as they are "in range" of the firing ship's weapons. This opens up a further question to consider - do your ships' weapons have one single range (e.g. 24" for autocannons, 36" for railguns, etc) or range bands whereby a ship's weapons become less likely to inflict damage the further out they are from an enemy ship?

Before you can consider your next two orders, you have to consider damage. How is damage tracked? Do ships have armor? Shields? Or is it just a straight "health" pool and the ship goes kablooey once it reaches zero?

"Release fighters" is where things start to get complicated, and where your two initial questions start to meld together. What kind of fighters should your ship release? When? How do you track damage to fighters? How do fighters deal damage to ships?
![](/assets/images/so-you-want-to-design-a-miniatures-wargame-part-2/imperial-starfleet-star.jpg)

The last order, "Repair", continues the damage questions from earlier. When a repair is triggered, how much damage should it heal (typically you'll figure out an answer to this in playtesting)?

As you ask yourself these questions, your gameplay loop starts to develop. You know what your core orders are, now you just have to figure out *when* those orders can be issued.

## Define An Order Of Operations

Almost all wargames have some kind of turn sequence and if they don't, have some way of trading off who is the active player. There are a number of ways your turn sequence can go: the old standby is IGOUGO, which, as the name implies, has one player complete all actions of a certain type before the other player can complete their actions. So the initiative player does all their movement, then the non-initiative player. Repeat for shooting and other phases.

Other options include alternating activation and alternating initiative:

- Alternating activation is when players alternate activating single units and completing some number of actions with that unit before initiative passes to the other player. So I might activate a frigate, move it 6" and then shoot at your destroyer. Once my shooting is done, initiative passes to you.
- Alternating initiative, on the other hand, plays a little differently. There is an "initiative player" and that player can activate any of their units as many times as they want until certain conditions are met, at which point the other player becomes the "initiative player".

Decide on the turn sequence that best fits the kind of game you're trying to write. It's totally fine to start your game with IGOUGO and switch over to alternating initiative in a later playtest. Speaking of playtesting...

## Playtest, playtest, playtest

Once you've decided on a turn sequence and fit your orders inside it, it's time to start playtesting your game. Get a friend and some ships together and run through an 'Annihilate' mission - this is the simplest way to get to grips with your core ruleset. Take notes and ask questions as you go. Some good starting questions to ask would be:

- Does the game consistently have interesting decisions to make?
- Can you explain why the victorious player won?
- How much time did you feel like you were playing for?
- Did the game feel like it was dragging?

The first two questions above are incredibly important because a game without interesting decisions quickly becomes boring and if players can't explain why they won (or why they lost), it may quickly lead to frustration, which quickly leads to abandoning the ruleset in favor of something else. The other two focus more on how much time the game takes to play - players won't want to play a game that drags, and IGOUGO systems in particular can feel like they drag because one player is literally waiting their turn.

As your ruleset becomes more mature, some follow-on questions to ask include:

- Did you notice any potentially game-breaking strategies?
- Did anyone do anything odd that might be an issue?

These questions focus primarily on game balance. A word to the wise: "balanced" does not mean "fair", especially in game design. All balance means is that if you have an X, you also have an anti-X. Balance is one of the first things players will look for when they start playtesting your game - if Alice can play the Empire and take 30 Star Destroyers and all I can take as the Rebellion are 10 X-Wings, not only is the game not *balanced*, it's also not *fun*. And building something fun is probably one of the core reasons you're reading this series in the first place.

I hope you enjoyed Part Two in this series. I'm not sure when Part Three will be out, so keep your eyes peeled!
