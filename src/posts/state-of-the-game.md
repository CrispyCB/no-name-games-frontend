---
title: What I Learned From My First Con
layout: layouts/post.html
coverImage: /assets/images/cover-images/StateOfTheGame.png
blurb: Or, four things you can do to make your first con appearance a good one.
date: 2023-07-26
---

Last Sunday, I went to my first-ever con as a vendor. Overall, I had a really good time, played some cool games, and even managed to get in some outreach about DEMIGOD while I was there. I also learned several things that will hopefully make my next vendor appearance even more successful than this one. 

1. Do adequate research about the con you're going to.

The con I went to on Sunday was called Harrisburg **Board Game** Day - emphasis mine. As someone trying to promote a miniatures wargame, I was definitely in the minority. There were some people demoing Warhammer 40,000 and Battletech, but the vast majority of people were demoing or playing actual, in-box board games. If I'd known that, I would have printed up and brought some copies of ["Wands & Laserguns"](https://shoutingcrow.itch.io/wands-and-laserguns) - a rules-lite, component-lite TTRPG that Justin Vandermeer and I created that probably would have been more of a hit than the relatively component-heavy DEMIGOD.

Additionally, do some research on the convention space itself. Will it have free-standing 6x4 tables? Booths? What will the lighting look like where your booth is? Can you get to it easily? Can you get supplies to it easily? All of these questions are important when it comes time to setting up for your first con.

2. Make sure you're put with people who have the same goals you do.

When I showed up to the convention hall, I was told that all of the playtesters were on the second floor. Which was fine, except that all the game demos (Warhammer 40,000/Battletech/etc) were happening on the *first* floor. The second floor, as it turned out, had spaces where people could demo games that they'd brought, but the vast majority of the space ended up being used by people renting games. It very much felt like the game designers were relegated to an afterthought - even when people got to the second floor, the first thing they saw was the giant booth full of rentable games, not so much the group of designers there to get people playing games they'd brought.

3. Make sure your game components are durable enough for a full day of gaming.

While this might go without saying, make sure that the components you bring with you can stand up to a full day of handling - not just by you, but by the general public, as well. While you and any friends you invite to playtest your designs might have the utmost respect for your components, the general public may not - and not even on purpose. I had the spears of two of the miniatures I brought with me snap off throughout the day I was at the convention, just from handling throughout the day. So making sure your components last the day is really important.

4. Bring adequate amounts of advertising

One of the biggest things that will bring people to your table is bright, eye-catching advertising. If you look at my table (below), you'll notice that the only real advertising I have is the NNG tablecloth and some business cards. 
![](/assets/images/state-of-the-game/table.jpg)

Because of the way the table is set up, you can't really see the tablecloth - something like a standing banner or table banner would do much better to attract people's attention by virtue of being seen from out in the aisle.

What tips do you have for people vending at their first con?