---
title: Handling Artillery In Modern Skirmish Wargames
layout: layouts/post.html
coverImage: /assets/images/cover-images/KingOfTheBattlefield.png
blurb: Artillery has been called the king of the battlefield, and it's typically what achieves the most kills in a modern battle. So how do we represent that in a skirmish game?
date: 2024-01-24
---
## The Nature Of Artillery

Before we can decide how to represent artillery in a tabletop wargame, first we have to settle on a definition of what artillery *is*. At its most basic, artillery is just a class of ranged weapons that launch munitions far beyond the range of human or powder-powered smallarms (bows, slings, crossbows, rifles, etc).

From its first incarnation as fairly-immobile siege engines used to breach enemy fortifications, artillery has evolved both in mobililty (horse-drawn and wheeled cannons all the way up to man-portable mortars) and lethality (solid shot all the way up to modern high-explosive rounds).

Since modern artillery generally fires high-explosive rounds unless directed otherwise, it can be assumed that the artillery a platoon or company commander would have access to would be firing this as well.

## Artillery As Area-of-Effect (AoE) Weapons

The use of high-explosive ammunition usually means that when artillery shows up in a tabletop wargame, its destructive power is represented by a *blast template* (shown below). Usually, larger artillery pieces such as towed guns use the 5" template, while smaller guns like mortars use the 3" template.
![](/assets/images/king-of-the-battlefield/blast_templates.png)
But why is this the way things are done?

Quite simply: because you need a way to designate artillery that is visible to both you and your opponent. But, if the rules for them are written correctly, templates can become much more than just a hammer of doom that hits your opponent. One way to handle this is to place the template during your turn, but have it "go off"/take effect during or after an opponent's movement phase.

Not only does it give your opponent a chance to react to the incoming template, but it also opens up tactical possibilities, as well. You can place templates to funnel enemies in a certain direction or deny them a section of the tabletop - both things that artillery is used for in real life.

## Artillery Without Templates?

But what if we don't want to use templates? They're one more thing to ask players to have on hand, they can get lost easily, etc. Instead, we can designate a square of some distance (Chain of Command uses 18", for example) and use *that* as your template.

Another way to handle it is to use the intrinsic bases of the troops themselves, like Crossfire does. The Crossfire rules essentially say "pick a squad, roll dice against them, any other squads within 1 stand/base-length are suppressed".

Whichever way you choose to handle it, your system needs some way to designate that every unit in a given space is being hit by incoming artillery.

## "Spread out - they got us zeroed!" - bunching up, AoE and morale

Being hit by incoming artillery tends to degrade unit morale, and units with degraded morale tend to bunch up, making them easy targets for AoE weapons like artillery and machine guns.

One way to represent this is by having the cohesion distance inside a unit decrease every time the unit fails a morale check, to the point where every model in the unit is base contact with every other model in the unit - a perfect target for a template.

Having unit cohesion slowly degrade as morale degrades ties into making morale failure a more gradual thing, which I discussed in my post on [handling morale in miniature wargames](/posts/handling-morale-in-miniature-wargames/) a couple months back.

## To recap

- AoE can give you tactical choices depending on how you employ it
- AoE templates can act like interactive terrain
- tie together AoE weapons, morale and unit cohesion.
