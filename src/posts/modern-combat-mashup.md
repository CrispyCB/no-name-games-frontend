---
title: Modern Combat Mashup - a first look at No Name Games' new project!
layout: layouts/post.html
coverImage: /assets/images/cover-images/ModernCombatMashup.png
blurb: Sorry for the hiatus! That said, I've got a new project in the works. Read on for more...
date: 2024-01-17
---

So I wanted to apologize for the second hiatus. To be honest, I didn't feel like writing blog posts. Or games, if I'm honest. You know that saying of "Find a job you love and you'll never work a day in your life"? Well, writing games was starting to feel a lot like work.

Now that I've taken a bit of a break, I'm back in business. Expect new blog posts, plus new game content. I'm about 40% done with "The Trials Of Herakles", and I'm hoping to get that out to playtesters by the end of this year (2024). I've also had a new game in the works. Well, for a given sense of "new" - it's a rewrite of a game I finished years ago and never released. That game was called "Humanity In Flames". It started life as a what-if scenario: I'd just finished Dan Abnett's "Embedded" and the way he'd written combat really stuck with me. I wanted to write a game that evoked the combat in that book - something frenetic and fast-paced.
![](/assets/images/modern-combat-mashup/embedded-front.png)
What I ended up coming up with could best be described as "[Force On Force](https://www.ambushalleygames.net) without all the charts." It combined basic versions of the unit profiles from Warhammer 40,000 4th edition with Force on Force's troop quality dice, and played much more like Warhammer 40,000 than it did Force on Force. Throw in Chain of Command's army list system, and you have the first version of Humanity In Flames. I eventually re-scaled the game into a 15mm company-level wargame, but that still didn't get me exactly where I wanted.

## Enter Covering Fire

From there, I went back to the drawing board. I re-examined what I wanted out of my game: fast-paced, frenetic combat that didn't worry about bean-counting. So I started poking around. I already knew [Chain of Command](https://toofatlardies.co.uk/product-category/chain-of-command/), but I wasn't a huge fan of its Chain of Command points. Eventually, I stumbled upon Covering Fire, a little two-page ruleset from the folks behind [One Page Rules](https://www.onepagerules.com/) (OPR). While it doesn't appear to still be available on their website, Covering Fire was OPR's take-off of Crossfire, a company-level WWII wargame written by Artie Conliffe in 1996. Covering Fire (and Crossfire before it) is designed to be a fast-paced game, for all that it's company-level. There are no turns. There is no measuring. Units move from terrain feature to terrain feature. Units accrue "pin markers" that are a representation of how combat effective they are - the more pin markers a unit has, the less effective they are, until they're killed.

## Making It My Own

I kept most of these features, excepting that units have to move from terrain feature to terrain feature - this makes for terrain-heavy boards, and not everyone has enough terrain to saturate a 6'x4' board. I added vehicle rules that are vaguely similar to Chain of Command's, but vastly simplified. I still have to flesh out those vehicle rules I mentioned above, plus make some changes to how infantry work (namely as individual models vs stands). I hope to document those rules for y'all in some upcoming blog posts, so stay tuned!

In the meanwhile, you can read those rules (tentatively titled "Eleven Bravo") [here](https://docs.google.com/document/d/1vIhxkRl_F2-Vyri-1Tn6J-ib7ldboMCcmoFeOnh9ThA/edit?usp=sharing).
