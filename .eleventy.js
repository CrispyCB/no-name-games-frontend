const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");
const pluginRss = require("@11ty/eleventy-plugin-rss");


module.exports = function(eleventyConfig) {
    eleventyConfig.addFilter("readingTime", function (text) {
        let content = new String(text);
        const speed = 240; // reading speed in words per minute
      
        // remove all html elements
        let re = /(&lt;.*?&gt;)|(<[^>]+>)/gi;
        let plain = content.replace(re, "");
      
        // replace all newlines and 's with spaces
        plain = plain.replace(/\s+|'s/g, " ");
      
        // create array of all the words in the post & count them
        let words = plain.split(" ");
        let count = words.length;
      
        // calculate the reading time
        let readingTime = Math.round(count / speed);
        if (readingTime === 0) {
          return "Less than 1 minute to read";
        } else if (readingTime === 1) {
          return "1 minute to read";
        } else {
          return readingTime + " minutes to read";
        }
    })
    eleventyConfig.addPassthroughCopy("src/css/");
    eleventyConfig.addPassthroughCopy("src/assets/images/");
    eleventyConfig.addPassthroughCopy("src/scripts/")
    eleventyConfig.addPassthroughCopy({ "src/assets/favicon": "/" });
    eleventyConfig.addPassthroughCopy("src/downloads/");
    eleventyConfig.addCollection("postsByYear", (collectionApi) => {
      const posts = collectionApi.getFilteredByGlob('src/posts/**/*.md').reverse();
      const years = posts.map(post => post.date.getFullYear());
      const uniqueYears = [...new Set(years)];
    
      const postsByYear = uniqueYears.reduce((prev, year) => {
        const filteredPosts = posts.filter(post => post.date.getFullYear() === year).reverse();
    
        return [
          ...prev,
          [year, filteredPosts]
        ]
      }, []);
    
      return postsByYear;
    });
    eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
    eleventyConfig.addPlugin(pluginRss);
    eleventyConfig.addWatchTarget("./src/css/");
    return {
        dir: {
        input: 'src',
        includes: '_includes',
        output: 'build'
    },
    templateFormats: ['md', 'njk', 'html'],
    markdownTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    };
}